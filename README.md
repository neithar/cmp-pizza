# README OF PIZZA APP #

This application was developed for a technical test

## Technologies ##

This Application is made with:
* AngularJS
* HTML
* CSS
* Bootstrap
* Custom bootstrap theme called bootswatch

## This Application allows you to: ##

*  View available pizzas
*  Create pizza
*  Update pizza
*  Delete pizza
*  Add ingredients to pizza
*  Delete ingredients to pizza

## Technical Documentation ##

### Controllers ###

* EditCtrl(edit.js): Controller for edit secction
* MainCtrl(main.js): Controller for list section
* NewCtrl(new.js): Controller for new section

### Views ###

* form.html: Edit pizza form view
* main.html: Show available pizzas in list
* new.html: Show new pizza form

### Services ###

* IngredientService: Service that interacts with the API to obtain information of ingredients
* PizzaService: Service that interacts with the API.

### Methods of Pizza Service ###

*   Get available pizzas
*   Get ingredients of single pizza
*  Update pizza information*
*  Delete pizza
*   Create pizza

### Other methods in PizzaService that not use API ###

   * removeIngredientPizza

### Repository owner ###

Sonia Benítez Martínez