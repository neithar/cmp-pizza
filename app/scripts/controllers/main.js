'use strict';

/**
 * @ngdoc function
 * @name cmpPizzaApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the cmpPizzaApp
 */
angular.module('cmpPizzaApp')
    .controller('MainCtrl', function ($scope, $http, $location, pizzaService) {
        $scope.pizzas = pizzaService.getPizzas();

        // Adds a pizza to the list of pizzas
        $scope.addPizza = function (pizza) {
            $location.path('/pizzas/new');
        }

        //Remove one pizza to the list of pizzas
        $scope.removePizza = function (pizza) {
            pizzaService.deletePizza(pizza);
        };
    });
