'use strict';

/**
 * @ngdoc function
 * @name cmpPizzaApp.controller:NewCtrl
 * @description Add new pizza
 * # NewCtrl
 * Controller of the cmpPizzaApp
 */
angular.module('cmpPizzaApp')
    .controller('NewCtrl', function ($scope, $http, $location, ingredientService, pizzaService) {
        var ingredientUrl = 'http://162.250.78.47/api/ingredients';
        var pizzaUrl = 'http://162.250.78.47/api/pizzas';
        $scope.tmpIngredient = {};
        $scope.newIngredient = null; // Our model value is null by default
        $scope.name = '';
        $scope.pizza = {
            ingredients: []
        };
        $scope.availableIngredients = ingredientService.getIngredients();

//        //Calls the API to update pizza's ingredients
//        $scope.createPizza = function () {
//            $http.put(pizzaUrl + '/' + pizzaId, $scope.pizza)
//                .success(function (data, status, headers, config) {
//                    alert("Pizza updated!");
//                    $location.path('/');
//                }).
//                error(function (data, status, headers, config) {
//                    alert("there was an error updating pizza!!");
//                });
//
//        };

        //Change radio button of selected ingredient
        $scope.changeRadio = function (ingredient) {
            $('span').css("display", "none");
            $('span[name="' + ingredient.name + '"]').css("display", "inline");
            $('input[name="ingredientSelected"]').val(JSON.stringify(ingredient));
        }

        //Adding ingredients to list
        $scope.addIngredient = function () {
            //Save information of current ingredient selected
            var avIngredient = JSON.parse($('input[name="ingredientSelected"]').val());
            var ul = document.getElementById("addIngredients");
            var li = document.createElement("li");
            li.appendChild(document.createTextNode(avIngredient.name));
            //Insert ingredient selected at the end of list of ingredients
            var inputIngredient = document.createElement("input");
            inputIngredient.setAttribute("id", avIngredient._id);
            inputIngredient.removeAttribute("ng-repeat");
            inputIngredient.setAttribute("name", avIngredient.name);
            inputIngredient.type = 'hidden';
            inputIngredient.value = avIngredient._id;
            ul.appendChild(li);
            li.appendChild(inputIngredient);
            //inputIngredient.append('<a class="delete-item pull-right" ng-click="removeIngredient(' + avIngredient._id + ', ' + $scope.pizza + ')"><i class="glyphicon glyphicon-trash"></i></a>');
            $scope.pizza.ingredients.push(avIngredient._id);
        }
        //Create pizza call service
        $scope.createPizza = function($event){
            pizzaService.newPizza($scope.name, $scope.pizza.ingredients);
        }


    });