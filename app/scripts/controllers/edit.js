'use strict';

/**
 * @ngdoc function
 * @name cmpPizzaApp.controller:EditCtrl
 * @description
 * # EditCtrl
 * Controller of the cmpPizzaApp
 */
angular.module('cmpPizzaApp')
    .controller('EditCtrl', function ($scope, $route, $routeParams, $http, $location, ingredientService, pizzaService) {
        var pizzaUrl = 'http://162.250.78.47/api/pizzas';
        var ingredientUrl = 'http://162.250.78.47/api/ingredients';
        //Get id of pizza by get
        var pizzaId = $routeParams.pizzaId;
        $scope.tmpIngredient = {};
        $scope.newIngredient = null; // Our model value is null by default

        var ingredients = [];
        //Get all available ingredients
        $scope.availableIngredients = ingredientService.getIngredients();


        //Get Pizza selected to show
        $http.get(pizzaUrl + '/' + pizzaId).
            success(function (data, status, headers, config) {
                $scope.pizza = data;
                //Check it's ingredients and save into ingredients array.
                if ($scope.pizza.hasOwnProperty("ingredients")) {
                    $scope.ingredients = pizzaService.getIngredientsPizza($scope.pizza);
                }
            }).
            error(function (data, status, headers, config) {
                alert("There was an internal error");
            });

        // Adds one ingredient to the list of pizzas
        $scope.addIngredient = function (ingredient) {
            $scope.ingredients.push(ingredient);
        }

        //Remove one ingredient single pizza
        $scope.removeIngredient = function (index, pizza) {
            //Call service to remove ingredient and update pizza
            pizza.ingredients = pizzaService.removeIngredientPizza(index, pizza);
            $scope.pizza = pizza;
            $scope.ingredients = pizzaService.getIngredientsPizza($scope.pizza);
        };

        //Change radio button of selected ingredient
        $scope.changeRadio = function (ingredient) {
            $('span').css("display", "none");
            $('span[name="' + ingredient.name + '"]').css("display", "inline");
            $('input[name="ingredientSelected"]').val(JSON.stringify(ingredient));
        }

        //Adding ingredients to list
        $scope.addIngredient = function () {
            //Save information of current ingredient selected
            var avIngredient = JSON.parse($('input[name="ingredientSelected"]').val());
            var ul = document.getElementById("addIngredients");
            var li = document.createElement("li");
            li.appendChild(document.createTextNode(avIngredient.name));
            //Insert ingredient selected at the end of list of ingredients
            var inputIngredient = document.createElement("input");
            inputIngredient.setAttribute("id", avIngredient._id);
            inputIngredient.setAttribute("name", avIngredient.name);
            inputIngredient.type = 'hidden';
            inputIngredient.value = avIngredient._id;
            ul.appendChild(li);
            li.appendChild(inputIngredient);
            //inputIngredient.append('<a class="delete-item pull-right" ng-click="removeIngredient(' + avIngredient._id + ', ' + $scope.pizza + ')"><i class="glyphicon glyphicon-trash"></i></a>');
            $scope.pizza.ingredients.push(avIngredient._id);
        }
        //Calls the API to update pizza's ingredients
        $scope.updatePizza = function () {
            pizzaService.updatePizzaData(pizzaId, $scope.pizza);
        };

    });
