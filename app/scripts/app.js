'use strict';

/**
 * @ngdoc overview
 * @name cmpPizzaApp
 * @description
 * # cmsPizzaApp
 *
 * Main module of the application.
 */
angular
  .module('cmpPizzaApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/pizzas/new', {
        templateUrl: 'views/new.html',
        controller: 'NewCtrl'
      })
      .when('/pizzas/edit/:pizzaId', {
        templateUrl: 'views/form.html',
        controller: 'EditCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
