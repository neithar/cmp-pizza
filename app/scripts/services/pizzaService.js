/**
 * @ngdoc function
 * @name cmpPizzaApp.factory:pizzas
 * @description Work with pizza list
 *
 */

angular.module("cmpPizzaApp")
    .factory("pizzaService", function ($http, $location, $route) {
        var tmpPizza = {};
//        var pizzaData = {};
        var pizzas = [];
        var pizzasUrl = 'http://162.250.78.47/api/pizzas';
        var pizzaIngredient = [];
        var ingredientUrl = 'http://162.250.78.47/api/ingredients';
        var pizzasLength = 0;
        return{
            //Retrieve all pizzas
            getPizzas: function () {
                pizzas = [];
                //Get all pizzas to show in main view
                $http.get(pizzasUrl).
                    success(function (data, status, headers, config) {
                        tmpPizza = data;
                        angular.forEach(tmpPizza, function (value, key) {
                            pizzas.push(value);
                        });
                    }).
                    error(function (data, status, headers, config) {
                        // called asynchronously if an error occurs
                        // or server returns response with an error status.
                    });
                pizzasLength = pizzas.length;
                return pizzas;
            },
            //Retrieve one single pizza --> Does not work :(
//            getSinglePizza: function(pizzaId){
//                //Get Pizza selected to show
//                $http.get(pizzasUrl + '/' + pizzaId).
//                    success(function (data, status, headers, config) {
//                        pizzaData = data;
//
//                    }).
//                    error(function (data, status, headers, config) {
//                        alert("There was an internal error");
//                    });
//                return pizzaData;
//            },
            getIngredientsPizza: function (pizza) {
                pizzaIngredient = [];
                if (pizza.hasOwnProperty("ingredients")) {
                    angular.forEach(pizza.ingredients, function (value, key) {
                        $http.get(ingredientUrl + '/' + value).
                            success(function (data, status, headers, config) {
                                pizzaIngredient.push(data);
                            }).
                            error(function (data, status, headers, config) {
                                alert("There was an internal error");
                            })

                    });
                    pizzaIngredient = pizzaIngredient.sort(function (a, b) {
                        return -( a.id - b.id || a.name.localeCompare(b.name) );
                    });
                    return pizzaIngredient;
                }
            },
            //Function to update pizza information
            updatePizzaData: function (pizzaId, pizzaObj) {
                $http.put(pizzasUrl + '/' + pizzaId, pizzaObj)
                    .success(function (data, status, headers, config) {
                        alert("Pizza updated!");
                        $location.path('/');
                    }).
                    error(function (data, status, headers, config) {
                        alert("there was an error updating pizza!!");
                    });
            },
            //Function to remove one ingredient of pizza
            removeIngredientPizza: function (ingredient, pizza) {
                var ingredients = pizza.ingredients;
                var filtered = ingredients.filter(function (element) {
                    return element !== ingredient;
                });
                return filtered;
            },
            //Function to remove one pizza
            deletePizza: function (pizza) {
                $http.delete(pizzasUrl + '/' + pizza._id)
                    .success(function (data, status, headers, config) {
                        alert("Pizza deleted!");
                        $route.reload();
                    }).error(function (data, status, headers, config) {
                        alert("there was an error updating pizza!!");
                    });
            },
            newPizza: function (name, ingredients) {
                //Create new object with data parameters
                var newPizza = {};
                newPizza.name = name;
                newPizza.ingredients = [];
                angular.forEach(ingredients, function (value, key) {
                    newPizza.ingredients.push(value);
                });
                $http.post(pizzasUrl, newPizza)
                    .success(function (data, status, headers, config) {
                        alert("Pizza created!");
                        $location.path('/');
                    }).
                    error(function (data, status, headers, config) {
                        alert("there was an error updating pizza!!");
                    });
            }
        }
    });