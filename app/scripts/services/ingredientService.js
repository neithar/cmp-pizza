/**
 * @ngdoc function
 * @name cmpPizzaApp.factory:ingredients
 * @description Works with ingredients lists
 *
 */

angular.module("cmpPizzaApp")
    .factory("ingredientService", function ($http) {
    var ingredientUrl = 'http://162.250.78.47/api/ingredients';
    var availableIngredients = [];
    $http.get(ingredientUrl)
        .success(function (data, status, headers, config) {
            angular.forEach(data, function (value) {
                availableIngredients.push(value);
            });
        }).
        error(function (data, status, headers, config) {
        });

    return{
        getIngredients: function () {
            return availableIngredients;
        }
    }
});
